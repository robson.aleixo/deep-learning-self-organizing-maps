import numpy as np
import matplotlib.pyplot as plt 
import pandas as pd 
import os

class som():
    '''
    It's used to clustering the datas, feature detection
    Category: Unsupervised
    '''
    def __init__(self, x, y, sc):

        self.x = x
        self.y = y
        self.sc = sc

    def training_som(self):
        '''
        Step 1: We start with a dataset composes of n_features independent variables
        Step 2: We create a grid composed of nodes, each one having a weight vector of n_features elements
        Step 3: Randomly initialize the values of the weight vectors to small numbers close to 0 (but not 0).
        Step 4: Select one random observation point from the dataset
        Step 5: Compute the Euclidean distances from this point to the different neurons in the network
        Step 6: Select the neuron that has the minimum distance to the point. This neuron is called the winning node.
        Step 7: Update the weights of the winning node to move it closer to the point
        Step 8: Using a Gaussian neighbourhood function of mean the winning node, also update the weights of the 
        winning node neighbours to move them closer to the point. The neighbourhood radius is the sigma in the Gaussian function
        Step 9: Repeat Steps 1 to 5 and update the weights after each observation (Reinforcement Learning) or after a batch
        of observations (Batch Learning), until the network converges to a point where the neighbourhood stops decreasing
        '''

        from Self_Organizing_Maps.minisom import MiniSom
        self.som = MiniSom(  x = 10, # number of rows by iteration
                        y = 10,
                        input_len = 15, 
                        sigma = 1.0, 
                        learning_rate= 0.5)
        
        self.som.random_weights_init(self.x)
        self.som.train_random(data = x, num_iteration = 100)

    def visualising(self):

        from pylab import bone, pcolor, colorbar, plot, show
        bone()
        pcolor(self.som.distance_map().T)
        colorbar()
        markers = ['o', 's']
        colors  = ['r', 'g'] 
        for i, j in enumerate(x):

            # Getting the winner node
            w = self.som.winner(j) 
            plot( w[0] + 0.5,
                  w[1] + 0.5,
                  markers[self.y[i]],
                  markeredgecolor = colors[y[i]], 
                  markerfacecolor = 'None',
                  markersize = 10,
                  markeredgewidth = 2
                  )
        show()

    def finding_fraud(self):

        mappings = self.som.win_map(self.x)
        frauds = np.concatenate((mappings[(8, 1)], mappings[(6, 8)]), axis = 0)
        frauds = self.sc.inverse_transform(frauds)
        print(frauds)

    def excecute(self):

        self.training_som()
        self.visualising()
        self.finding_fraud()


if __name__ == '__main__':

    file_path = os.path.join(os.getcwd(), 'secao_4 - SELF ORGANIZING MAPS (SOM)')
    file_train_path = os.path.join(file_path, 'Credit_Card_Applications.csv')

    # Importind the dataset
    dataset = pd.read_csv(file_train_path)
    x = dataset.iloc[:, :-1].values
    y = dataset.iloc[:, -1].values

    # Feature Scaling
    from sklearn.preprocessing import MinMaxScaler
    sc = MinMaxScaler(feature_range = (0,1))
    x = sc.fit_transform(x)

    som = som(x, y, sc)
    som.excecute()


